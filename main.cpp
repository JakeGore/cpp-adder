#include <iostream>
#include "BitAdder.hpp"

int main()
{
	BitAdder bitAdder;
	int numbers[2];

	std::cout << "Enter the first number: ";
	std::cin >> numbers[0];

	std::cout << "Enter the second number: ";
	std::cin >> numbers[1];

	int totalSum = 0, currentCarry = 0;

	for (int i = 0; i < 8; i++) {
		bool bits[2];
		bits[0] = (numbers[0] >> i) & 0x1;
		bits[1] = (numbers[1] >> i) & 0x1;
		bitAdder.execute(bits[0], bits[1], currentCarry);
		totalSum |= (bitAdder.getSum() << i);
		currentCarry = bitAdder.getCarry();
	}

	std::cout << "Answer is: " << totalSum << std::endl;

	return 0;
}