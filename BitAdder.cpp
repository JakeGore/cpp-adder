#include "BitAdder.hpp"

void BitAdder::execute(bool a, bool b, bool c)
{
	sum = (a ^ b) ^ c;
	carry = (a & b) | ((a ^ b) & c);
}

bool BitAdder::getSum()
{
	return sum;
}

bool BitAdder::getCarry()
{
	return carry;
}