#ifndef BITADDER_H
#define BITADDER_H

class BitAdder
{
private:
	bool sum;
	bool carry;
public:
	void execute(bool a, bool b, bool c);
	bool getSum();
	bool getCarry();
};

#endif